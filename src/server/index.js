const Hapi = require('@hapi/hapi');
const routes = require('./routes/index.js');
module.exports = class FlipstarterHttpServer {
  
  /**
   * FlipstarterHttpServer
   * 
   * @param {import('../core/index.js')} backend Backend
   */
  constructor(ipfs, baseClientCid) {
    this._ipfs = ipfs;
    this._baseClientCid = baseClientCid;
  }

  async start({ port, host }, ...hapiParams) {

    this.server = await this._createServer({ port, host, ipfs: this._ipfs, baseClientCid: this._baseClientCid });
    await this.server.start(...hapiParams)
  }

  async _createServer({ port, host, ipfs, baseClientCid }) {

    const server = Hapi.server({ 
      port, 
      host,
    });
    
    server.route(routes);

    server.app.ipfs = ipfs;
    server.app.baseClientCid = baseClientCid;

    return server;
  }

  async stop() {
    await this.server.stop();
    this.server = null;
  }
};