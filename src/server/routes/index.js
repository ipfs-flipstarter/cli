const add = require("./add");
const refs = require("./refs");

module.exports = [
  add,
  refs
]