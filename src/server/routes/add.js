const { createFlipstarterCampaignHtml, createFlipstarterCampaignSite } = require('@ipfs-flipstarter/core/');
const { schema: campaignSchema } = require("../utils/campaign");

module.exports = {
  options: {
    validate: {
      failAction: (req, h, err) => {
        throw err;
      },
      options: {
        allowUnknown: true,
        stripUnknown: false
      },
      payload: campaignSchema,
    },
    cors: true
  },
  method: 'POST',
  path: '/campaign/add',
  handler: async function (request) {

    /** @type {import('ipfs').IPFS} */
    const ipfs = request.server.app.ipfs;
  
    //TODO God willing: validate campaign (like size)
    const campaign = request.payload;

    const baseClientCid = request.server.app.baseClientCid;
    const baseClientHtml = request.server.app.baseClientHtml;

    try {

      const indexPageHtml = await createFlipstarterCampaignHtml(baseClientHtml, campaign)
      const hash = await createFlipstarterCampaignSite(ipfs, baseClientCid, indexPageHtml, campaign);
      const response = { hash }
      
      const gatewayUrl = request.server.app.gatewayUrl;
      if (gatewayUrl) {
        response.gatewayUrl = gatewayUrl;
      }

      return response;
    
    } catch (err) {
      console.log(err);
      throw new Error("Failed to create campaign");
    }
  }
}