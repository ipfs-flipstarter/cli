const fs = require('fs');
const os = require('os');
const path = require('path');

const { debug } = require('debug');
const log = debug('@ipfs-flipstarter/backend:cli-utils');

const FlipstarterHttpBackend = require('../server/index.js');
const { create } = require('@ipfs-flipstarter/http-client/index.js');
// const FlipstarterCore = require('/core/index.js');

const getRepoPath = () => {
  return process.env.FLIPSTARTER_PATH || path.join(os.homedir(), '/.flipstarters');
}

const isDaemonOn = () => {
  try {
    fs.readFileSync(path.join(getRepoPath(), 'api'))
    log('daemon is on')
    return true
  } catch (/** @type {any} */ err) {
    log('daemon is off')
    return false
  }
}

async function getBackend (argv) {
  
  if (!argv.api && !isDaemonOn()) {

    //TODO God willing: implement a core api that can be used for 
    //1. admin (create a new flipstarter, possibly have multiple running at same time using electrum/ipfs, on backend/cli and web)
    //    also creating a campaign at all via markdown and what not for both.
    //2. client (view a campaign, pass in a campaign.json and get campaign data, iA)
    throw new Error("No non-daemon commands allowed");

    const backend = await (new FlipstarterCore().init({
      silent: argv.silent,
      repoAutoMigrate: argv.migrate,
      repo: getRepoPath(),
      init: { allowNew: false },
      start: false,
      pass: argv.pass
    }));

    return {
      isDaemon: false,
      backend,
      cleanup: async () => {
        await backend.stop()
      }
    }

  } else {

    let endpoint = null
    if (!argv.api) {
      const apiPath = path.join(getRepoPath(), 'api')
      try {
        endpoint = JSON.parse(fs.readFileSync(apiPath).toString());
      } catch (err) {
        throw new Error("Error parsing repo", apiPath);
      }
    } else {
      //TODO God willing: make sure matches what JSON data supplies, God willing.
      endpoint = argv.api
    }

    const backend = await create({ endpoint });
    
    return {
      isDaemon: true,
      backend,
      cleanup: async () => { }
    }
  }
}


let visible = true
function disablePrinting() { visible = false }

/**
 * @type {import('./types').Print}
 */
function print(msg, includeNewline = true, isError = false) {
  if (visible) {
    if (msg === undefined) {
      msg = ''
    }
    msg = msg.toString()
    msg = includeNewline ? msg + '\n' : msg
    const outStream = isError ? process.stderr : process.stdout

    outStream.write(msg)
  }
}

print.clearLine = () => {
  return process.stdout.clearLine(0)
}

/**
 * @param {number} pos
 */
print.cursorTo = (pos) => {
  process.stdout.cursorTo(pos)
}

/**
 * Write data directly to stdout
 *
 * @param {string|Uint8Array} data
 */
print.write = (data) => {
  process.stdout.write(data)
}

/**
 * Print an error message
 *
 * @param {string} msg
 * @param {boolean} [newline=true]
 */
print.error = (msg, newline = true) => {
  print(msg, newline, true)
}

// used by ipfs.add to interrupt the progress bar
print.isTTY = process.stdout.isTTY
print.columns = process.stdout.columns

module.exports = {
  getRepoPath,
  isDaemonOn,
  getBackend,
  print,
  disablePrinting
}