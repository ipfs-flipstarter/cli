const FlipstarterServer = require('../../server/index.js');
const Ipfs = require('ipfs');
const IpfsHttpClient = require('ipfs-http-client');
const last = require("it-last");
const fs = require('fs');
const glob = require('glob');
const path = require('path');

const clientBaseDir = path.join(
  path.dirname(require.resolve('@ipfs-flipstarter/client/package.json')),
  "dist"
);

/** 
 * @param {Ipfs.IPFS} ipfs
 * @returns {Promise<import('multiformats').CID>} cid
 */
async function loadClientBaseDir(ipfs) {
  return (await last(ipfs.addAll((async function * getClientFiles() {
    const files = glob.sync("**/*", {
      cwd: clientBaseDir,
      nodir: true
    })
    
    for (var relativePath of files) {
      
      const absPath = path.resolve(clientBaseDir, relativePath);
      yield {
        path: relativePath,
        content: await fs.promises.readFile(absPath)
      }
    }
  })(), { wrapWithDirectory: true }))).cid;
}

module.exports = {
  command: 'daemon [port] [host]',
  describe: 'start flipstarter backend daemon',
  builder: {
    port: {
      alias: 'p',
      default: 8082
    },
    host: {
      alias: 'h',
      default: 'localhost'
    },
    api: {
      default: undefined,
    }
  },
  async handler (argv) {
    const { print, repoPath, isDaemon } = argv.ctx

    if (isDaemon) {
      throw new Error("daemon already started");
    }

    /** @type {Ipfs.IPFS} */
    const ipfs = argv.api ? IpfsHttpClient.create(argv.api) : await Ipfs.create();

    const baseClientCid = await loadClientBaseDir(ipfs);
    const server = new FlipstarterServer(ipfs, baseClientCid);

    const host = argv.host || '127.0.0.1';
    const port = argv.port || '8082';
    const options = { host, port };

    try {
      
      await server.start(options);
      
      if (argv.verbose) {
        console.info(`server started on ${host}:${port}`);
      }

    } catch (err) {

      console.log("failed to start the http server");
      
      if (argv.verbose) {
        console.log(err);
      }

      return false;
    }

    print('Daemon is ready')

    const cleanup = async () => {
      print('Received interrupt signal, shutting down...')
      await server.stop()
      print('Server stopped cleanly')
      process.exit(0)
    }

    // listen for graceful termination
    process.on('SIGTERM', cleanup)
    process.on('SIGINT', cleanup)
    process.on('SIGHUP', cleanup)
  }
}
