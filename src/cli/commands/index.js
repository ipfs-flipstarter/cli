const daemon = require("./daemon");
const add = require("./add");

module.exports = [
  daemon,
  add,
]